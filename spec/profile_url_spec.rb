#encoding: utf-8
require 'spec_helper'

describe ProfileUrl do

  TWITTER_PREFIX = "https://twitter.com/"
  FACEBOOK_PREFIX = "https://facebook.com/"
  GITHUB_PREFIX = "https://github.com/"
  SOUNDCLOUD_PREFIX = "https://soundcloud.com/"

  it "must raise error if name is not supported" do
    expect { rebuild_model(User, :stackoverflow) }.to raise_error
  end

  it "must get profile from default source" do
    rebuild_model(User, :twitter)
    @user = User.create!(name: "rafal", profile_url: "twitterlink")

    @user.twitter.url.should == TWITTER_PREFIX + "#{@user.profile_url}"
  end

  it "must get profile from source from options" do
    rebuild_model(User, :facebook, source: "facebook_link")
    @user = User.new(name: "rafal", facebook_link: "facebooklink")

    @user.facebook.url.should == FACEBOOK_PREFIX + "#{@user.facebook_link}"
  end

  it "must get profile for github" do
    rebuild_model(User, :github)
    @user = User.new(name: "rafal", profile_url: "github")

    @user.github.url.should == GITHUB_PREFIX + "#{@user.profile_url}"
  end

  it "must get profile for soundcloud" do
    rebuild_model(User, :soundcloud)
    @user = User.new(name: "rafal", profile_url: "rafał-trojanowski")

    @user.soundcloud.url.should == SOUNDCLOUD_PREFIX + "#{@user.profile_url}"
  end

  context 'facebook' do
    it "should do NOT add prefix if not needed" do
      rebuild_model(User, :facebook)

      links = [
        "suffix",
        "facebook.com/suffix",
        "www.facebook.com/suffix",
        "http://www.facebook.com/suffix",
        "https://www.facebook.com/suffix",
        "http://facebook.com/suffix",
        "https://facebook.com/suffix"
      ].each do |link|
        @user = User.new(name: "rafal", profile_url: link)

        @user.facebook.url.should == "https://facebook.com/suffix"

      end
    end
  end
end
