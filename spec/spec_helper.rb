require 'profile_url'
require 'active_record'

ActiveRecord::Base.establish_connection adapter: "sqlite3", database: ":memory:"

ActiveRecord::Schema.define do
  self.verbose = false

  create_table :users, :force => true do |t|
    t.string :name
    t.string :profile_url
    t.string :facebook_link
    t.string :github
    t.timestamps
  end
end

class User < ActiveRecord::Base
  extend ProfileUrl::ClassMethods
end

def rebuild_model(klass, name, options = {})
  rebuild_class klass, name, options
end

def rebuild_class klass, name, options = {}
  class_name = klass.to_s

  reset_class(class_name).tap do |klass|
    klass.has_profile_on name, options
  end
end

def reset_class class_name
  ActiveRecord::Base.send(:include, ProfileUrl::Glue)
  Object.send(:remove_const, class_name) rescue nil
  klass = Object.const_set(class_name, Class.new(ActiveRecord::Base))

  klass
end
