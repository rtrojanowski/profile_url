module ProfileUrl
  class Profile
    def self.default_options
      @default_options ||= {
                              source: "profile_url"
                           }
    end

    def initialize(name, instance, options)
      @name, @instance = name, instance

      @options = self.class.default_options.merge(options)
    end

    def url(options = {})
      default_options = {:source => @options[:source]}

      @url_generator = UrlGenerator.new(self, options)
      @url_generator.for(@name, default_options.merge(options))
    end
  end
end
