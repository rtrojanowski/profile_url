module ProfileUrl
  class HasProfileOn
    def self.supported_services
      %w(facebook twitter github soundcloud)
    end

    def self.has_support_for?(service_name)
      supported_services.include? service_name.to_s
    end

    def self.define_for(klass, service_name, options)
      raise "Not supported" unless has_support_for?(service_name)

      new(klass, service_name, options).define
    end

    def initialize(klass, service_name, options) 
      @klass, @service_name, @options = klass, service_name, options
    end

    def define
      define_instance_getter
    end

    def define_instance_getter
      service_name, options = @service_name, @options

      @klass.send :define_method, @service_name do
        ivar = "@profile_#{service_name}"
        profile = instance_variable_get(ivar)

        if profile.nil?
          profile = Profile.new(service_name, self, options)
          instance_variable_set(ivar, profile)
        end

        profile
      end
    end
  end
end
