require 'profile_url'

module ProfileUrl
  require 'rails'

  class Railtie < Rails::Railtie
    initializer 'profile_url.insert_into_active_record' do |app|
      ActiveSupport.on_load :active_record do
        ProfileUrl::Railtie.insert
      end
    end
  end

  class Railtie
    def self.insert
      if defined?(ActiveRecord) 
        ActiveRecord::Base.send(:include, ProfileUrl::Glue)
      end
    end
  end
end
