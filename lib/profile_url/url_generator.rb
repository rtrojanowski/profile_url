module ProfileUrl
  class UrlGenerator
    FACEBOOK_REGEX = /\A((https?)\:\/\/)?(www\.)?((facebook\.com)\/[^\?](\A?)[^\/]+)\Z/
    TWITTER_REGEX = /\A(https?:\/\/)?((w{3}\.)?)twitter\.com\/[a-zA-Z0-9\_]+\Z/
    GITHUB_REGEX = /\A(https?:\/\/)?((w{3}\.)?)github\.com\/[a-zA-Z0-9]+[a-zA-Z0-9\-]+\Z/
    SOUNDCLOUD_REGEX = /\A(https?:\/\/)?((w{3}\.)?)github\.com\/[a-zA-Z0-9]+[a-zA-Z0-9\-]+\Z/

    def initialize(profile, profile_options)
      @profile = profile
      @profile_options = profile_options
    end

    def for(name, options)
      regex = "#{name.upcase}_REGEX"
      object = @profile.instance_variable_get("@instance") # TODO: refractor
      nickname = object.send(options[:source])

      nickname = nickname.split("/").last if nickname =~ UrlGenerator.const_get(regex)

      ["https://#{name}.com", nickname].compact.join('/')
    end
  end
end
