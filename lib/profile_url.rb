require "profile_url/version"
require 'profile_url/has_profile_on'
require 'profile_url/url_generator'
require 'profile_url/profile'
require 'profile_url/glue'
require "profile_url/railtie" if defined? Rails

module ProfileUrl

  def self.options
    @options ||= {
    }
  end

  module ClassMethods
    def has_profile_on(name, options = {})
      HasProfileOn.define_for(self, name, options)
    end
  end

end
