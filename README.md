# ProfileUrl

Experimental gem that provides functionality to detect and return social profile url from model attribute, and validate them too. highly inspired paperclip and validates_formatting_of. An update to https://github.com/rafaltrojanowski/validates_profile

TODO: optional validation

## Installation

Add this line to your application's Gemfile:

    gem 'profile_url'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install profile_url

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request